package my_pack;


/*
Написать рекурсивную функцию для вычисления индекса максимального
элемента массива из n элементов.
 */

public class TaskCh10N049 {

    public static void main(String[] args) {
        int[] array = {250, 500, 300, 400};
        int indexMaxElement = findIndexOfMaxElement(array, array.length - 1);
        System.out.println("The index of the max element = " + indexMaxElement);
    }


   private static int findIndexOfMaxElement(int[] array, int current) {
        int indexMax;
        int tempIndex = current - 1;
        if (tempIndex == 0)
        {
            return tempIndex;
        } else {
            indexMax = findIndexOfMaxElement(array, tempIndex);
            return array[tempIndex] > array[indexMax] ? tempIndex : indexMax;
        }
    }
}