package my_pack;
/*
Object class for the interviewer
 */

import my_pack.human;

public class interviewer implements human {

    /*
     Methods implemented by human interface
      */

    //salute

    public String salute() {
        String greet = "Hi! ";
        return greet;
    }

    //next clause

    public String clause() {
        String say = "Introduce yourself and describe your java experience please";
        return say;
    }

    //Printing saying in the console

    public void speak() {
        System.out.println(salute() + clause());
    }
    
}
