package my_pack;

/*
Object class for the self learner
 */

import my_pack.candidate;

public class selfLearner extends candidate {
    

    //constructor to initialize  the name

    public selfLearner(String name) {
        super(name);
    }
    
/*
Methods implemented by human interface
 */

    //salute with the name

    public String salute() {
        StringBuilder greet = new StringBuilder();
        greet.append("Hi! my name is ");
        greet.append(name);
        return greet.toString();
    }
    
    //specific clause for the self learner

    public String clause() {
        String say = "\n I have been learning java by myself, nobody examined"
                + "how thorough is my knowledge and how good is my code.";
        return say;
    }
    
    //printing the both the clauses in the console

    public void speak() {
        System.out.println(salute() + clause());
    }

}
