package my_pack;

import java.util.Scanner;

/*
Составить программу, которая печатает заданное слово, начиная с последней буквы.
use StringBuilder
 */

public class TaskCh09N042 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a word");
        String word = scanner.next();
        System.out.println(findReversedWord(word));
    }

   private static String findReversedWord(String word) {
        return new StringBuilder(word).reverse().toString();
    }
}
