package my_pack;

import java.util.Scanner;
/*
Напечатать таблицу перевода 1, 2, ... 20 долларов США в рубли по текущему курсу (значение курса вводится с клавиатуры).
 */

public class TaskCh05N010 {

  public static void main(String[] args) {
    System.out.println("Please enter the exchange course:");
    Scanner scanner = new Scanner(System.in);
    Double courseOfExchange = scanner.nextDouble();
    int count = 0;
    for (Double i : getCourseOfExchange(courseOfExchange)) {
      System.out.println(++count + " " + i);
    }
  }

  private static Double[] getCourseOfExchange(double x) {
    Double[] convertDollarsToRub = new Double[20];
    int i = 0;
    while (i < convertDollarsToRub.length) {
      i++;
      convertDollarsToRub[i - 1] = i * x;
    }
    return convertDollarsToRub;
  }
}


