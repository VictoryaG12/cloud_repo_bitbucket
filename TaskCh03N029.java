package my_pack;

import java.util.Scanner;

/*
 * Записать условие, которое является истинным, когда:
 * а) каждое из чисел X и Y нечетное;
 * б) только одно из чисел X и Y меньше 20;
 * в) хотя бы одно из чисел X и Y равно нулю;
 * г) каждое из чисел X, Y, Z отрицательное;
 * д) только одно из чисел X, Y и Z кратно пяти;
 * е) хотя бы одно из чисел X, Y, Z больше 100.
 * don't use *+-"/" operators,
 * in (b) solve with <=2 and in d - with <=3 comparison operators;
 * don't use extra parentheses!
 * write task's test as javadoc for each solution;
 */

public class TaskCh03N029 {

    public static void main(String[] args) {
        System.out.println("Please input three numbers X,Y,Z, each on a new line");
        Scanner scanner = new Scanner(System.in);
        int X = scanner.nextInt();
        int Y = scanner.nextInt();
        int Z = scanner.nextInt();
        System.out.println(ProblemA (X,Y));
        System.out.println(ProblemB (X,Y));
        System.out.println(ProblemV (X,Y));
        System.out.println(ProblemG (X,Y,Z));
        System.out.println(ProblemD (X,Y,Z));
        System.out.println(ProblemE (X,Y,Z));
    }

    /*
     * каждое из чисел X и Y нечетное
     */

    static boolean ProblemA(int X, int Y) {
        return X % 2 != 0 && Y % 2 != 0;
    }

    /*
     * только одно из чисел X и Y меньше или равно 2.
     */
     static boolean ProblemB(int X, int Y) {
        return X <= 2 ^ Y <= 2;
    }

    /*
     * хотя бы одно из чисел X и Y равно нулю
     */

     static boolean ProblemV(int X, int Y) {
        return X == 0 || Y == 0;
    }

    /*
     * каждое из чисел X, Y, Z отрицательное
     */
     static boolean ProblemG(int X, int Y, int Z) {
        return X < 0 && Y < 0 && Z < 0;
    }

    /*
     * только одно из чисел X, Y и Z меньше или равно 3.
     */
    static boolean ProblemD(int X, int Y, int Z) {
        return (X <= 3) ^ (Y <= 3) ^ (Z <= 3) ;
    }
  
    /*
     *хотя бы одно из чисел X, Y, Z больше 100
     */
     static boolean ProblemE(int X, int Y, int Z) {
        return X > 100 || Y > 100 || Z > 100;
    }
}
