package my_pack;

import java.util.Scanner;

/*
Дано слово. Верно ли, что оно начинается и оканчивается на одну и ту же букву?
 */
public class TaskCh09N017 {

    public static void main(String[] args) {
        System.out.println("Please type your word:");
        Scanner scanner = new Scanner(System.in);
        String inputWord = scanner.next();
        System.out.println(IsFirstLastCharSame(inputWord));
    }

   private static boolean IsFirstLastCharSame(String inputWord) {
        return (inputWord.toLowerCase().charAt(0) == inputWord.toLowerCase().charAt(inputWord.length() - 1));
    }
}


