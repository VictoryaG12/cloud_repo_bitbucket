package my_pack;

/*
 *Дано трехзначное число. Найти число, полученное при прочтении его цифр
 *справа налево. Число 100 < n < 200 .
 */

public class TaskCh02N013 {

    public static void main(String[] args) {
            int reversedNumber = reversedNumber(125);
            printOutput(reversedNumber);
        }

    private static int reversedNumber(int inputNumber) {
        int firstDigit = inputNumber % 10;
        int secondDigit = inputNumber % 100 / 10;
        int thirdDigit = inputNumber / 100;

        return (firstDigit * 100) + (secondDigit * 10) + thirdDigit;
    }

    private static void printOutput(int number) {

        System.out.println(number);
    }

}



