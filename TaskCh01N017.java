package my_pack;

import static java.lang.Math.*;

// о, п, р, с (use methods of java.lang.Math class)

public class TaskCh01N017 {

    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        int c = 30;
        int x = 40;

        double outputO = getOutputO(x);
        double outputP = getOutputP(a, b, c, x);
        double outputR = getOutputR(x);
        double outputC = getOutputC(x);

      printOutput(outputO);
      printOutput(outputP);
      printOutput(outputR);
      printOutput(outputC);
    }

  private static double getOutputO(int x) {
        return sqrt(1 - pow(sin(x), 2));
    }

    private static double getOutputP(int a, int b, int c, int x) {
      return 1 / (sqrt(a * pow(x, 2) + b * x + c));
    }

     private static double getOutputR(int x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x));
    }

    private static double getOutputC(int x) {
        return abs(x) + abs(x + 1);
    }

     private static void printOutput(double output) {
        System.out.println(output);
    }

}



