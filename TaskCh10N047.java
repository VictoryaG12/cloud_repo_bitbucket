package my_pack;

import java.util.Scanner;
/*
Написать рекурсивную функцию для вычисления k-го члена последовательности Фибоначчи.
 Последовательность Фибоначчи f1, f2, ... образуется по
закону: f1 1; f2 1; fi fi 1 fi 2 ( i 3, 4, ...).
 */

public class TaskCh10N047 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter value of K");
        int elementK = scanner.nextInt();
        int K =calculateValueKFibonacci(elementK);
        System.out.println("The K element =" + K);

    }

    private static int calculateValueKFibonacci (int elementK) {
        if(elementK == 1 || elementK == 2)
        {
            return 1;
        }
        return  calculateValueKFibonacci(elementK - 1) + calculateValueKFibonacci(elementK - 2);
    }
}