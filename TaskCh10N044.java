package my_pack;

import java.util.Scanner;

/*
Написать рекурсивную функцию нахождения цифрового корня натурального числа.
 */
public class TaskCh10N044 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Please enter a number");
    int number = scanner.nextInt();
    int root = calculateDigitRoot(number);
    System.out.println("The digital root of a number is:=" + root);
  }

  private static int calculateDigitRoot(int number) {
    if (number < 10) {
      return number;
    } else {
      return calculateDigitRoot(sumOfDigits(number));
    }
  }

  private static int sumOfDigits(int number) {
    if (number < 10) {
      return number;
    } else {
      return sumOfDigits(number / 10) + number % 10;
    }
  }
}
