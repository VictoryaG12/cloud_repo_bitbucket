package my_pack;

/*
interface for all the characters involved in the interview
 */

public interface human {

	//salute clause
     String salute();

    //other remaining clause
   String clause();

    //printing in the console
     void speak();
}
