package my_pack;

/*
Abstract class implemented for the use of both graduate and self-Learner
 */

public abstract class candidate implements human {
    
    //Name of the candidate (Graduate or the self-Learner)

    public String name;     
    
    //Constructor with initializing name

    public candidate(String name){
        this.name = name;
    }

    /*
    Methods implemented by human interface
     */

    public String salute() {
        throw new UnsupportedOperationException("Not supported yet.");

    }

    public String clause() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    

    public void speak() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
}
