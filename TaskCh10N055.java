package my_pack;

import java.util.Scanner;
/*
Написать рекурсивную процедуру перевода натурального числа из десятичной системы счисления в N-ричную.
Значение N в основной программе вводится с клавиатуры (2 <= N <= 16).
don't use jdk utils and if/switch to map A-F letters
 */

public class TaskCh10N055 {

    private static char charSymbols[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E',
      'F'};

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the number");
        int number = scanner.nextInt();
        System.out.println("Please enter scale of notation");
        int N = scanner.nextInt();
        System.out.println(transferFromDecimalToN(number, N, " "));
    }

    private static String transferFromDecimalToN(int num, int n, String result) {
        int s = num % n;
        int res = num / n;

        if (res > 0) {
            return transferFromDecimalToN (res, n, charSymbols[s] + result);
        }

        return charSymbols[s] + result;
    }
}