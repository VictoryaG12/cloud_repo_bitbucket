package my_pack;

import java.util.Scanner;

/*
Дано слово. Поменять местами первую из букв а и последнюю из букв о. Учесть возможность того, что таких букв в
слове может не быть.
 */

public class TaskCh09N107 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Please enter a word");
    String inputWord = scanner.next();
    System.out.println(changeLetterAToO(inputWord));
  }

  private static String changeLetterAToO(String inputWord) {
    if (!inputWord.toLowerCase().contains("a") || !inputWord.toLowerCase().contains("o")) {
      System.out.println("Your word does not contain letters a or o");
    } else {
      String modifiedWord= inputWord.replaceFirst("a", "o");
      modifiedWord = modifiedWord.substring(0, modifiedWord.lastIndexOf("o")) + "a" +
        modifiedWord.substring(modifiedWord.lastIndexOf("o") + 1, modifiedWord.length());

      return modifiedWord;
    }
    return inputWord;
  }
}





