package my_pack;

import java.util.Scanner;

/*
 * В трехзначном числе x зачеркнули его вторую цифру. Когда к образованному
 * при этом двузначному числу справа приписали вторую цифру числа x, то получилось число n.
 * По заданному n найти число x.
 * число n,   100 ≤ n ≤ 999 .
 */

public class TaskCh02N031 {

  public static void main(String[] args) {
    int numberX = findNumberX(numberN());
    printResult(numberX);
  }

  private static int numberN() {
    System.out.println("Enter the number n in a range 100<=n<=999 :");
    Scanner scanner = new Scanner(System.in);
    int number = scanner.nextInt();
    return number;
  }

  private static int findNumberX(int originalNumber) {
    int digitThree = originalNumber % 10;
    int digitTwo = originalNumber % 100 / 10;
    int digitOne = originalNumber / 100;
    return ((digitOne * 100) + (digitThree * 10) + digitTwo);
  }

  private static void printResult(int numberX) {

    System.out.println("x = " + numberX);
  }
}