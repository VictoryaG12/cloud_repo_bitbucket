package my_pack;

import java.util.Arrays;

   /*
   В двумерном массиве хранится информация о количестве учеников в том или ином классе каждой параллели школы с первой
   по одиннадцатую (в первой строке — информация о количестве учеников в первых классах, во вто-рой — о вторых и т. д.)
    В каждой параллели имеются 4 класса. Определить среднее количество учеников в классах каждой параллели.
    */
   public class TaskCh12N063 {
       public static void main( String[] args) {
               int[][] schoolStudentsArray = {
                 {35, 25, 24, 21},
                 {20, 40, 33, 32},
                 {20, 25, 25, 20},
                 {35, 26, 20, 28},
                 {25, 28, 29, 40},
                 {27, 38, 31, 32},
                 {26, 21, 27, 28},
                 {25, 27, 23, 24},
                 {32, 34, 35, 36},
                 {28, 32, 25, 15},
                 {33, 32, 41, 45}};

               int[] result = schoolMean(schoolStudentsArray);
               System.out.println(Arrays.toString(result));
           }

           private static int[] schoolMean(int[][] schoolStudentsArray) {
               int[] averageSchoolStudentsArray = new int[11];
               for (int i = 0; i < schoolStudentsArray.length; i++) {
                   int sum = 0;
                   for (int j = 0; j < schoolStudentsArray[i].length; j++) {
                       sum += schoolStudentsArray[i][j];
                       averageSchoolStudentsArray[i] = sum / schoolStudentsArray[i].length;
                   }
               }
               return averageSchoolStudentsArray;
           }
       }