package my_pack;

import java.util.Scanner;
/*
Строка содержит арифметическое выражение, в котором используются круглые скобки, в том числе вложенные. Проверить,
правильно ли в нем расставлены скобки.
 */

public class TaskCh09N185 {

    public static void main(String[] args) {

            System.out.println("Please write an arithmetic expressions with parentheses:");
            Scanner scanner = new Scanner(System.in);
            String mathExpression = scanner.next();
            System.out.println (checkMathBrackets(mathExpression));
        }

    private static String checkMathBrackets(String mathExpression) {
        String checkMathExpression = "";
        int counter = 0;

        for (int i = 0; i < mathExpression.length(); i++) {
            if ("(".equals(mathExpression.substring(i, i + 1))) {
                counter++;
            }

            if (")".equals(mathExpression.substring(i, i + 1))) {
                counter--;
            }
        }

        if (counter == 0) {
            checkMathExpression = "yes";
        }
        if (counter < 0) {
            System.out.println("The mathexpression has redundant right brackets in" + " " + (mathExpression.indexOf
              (")") + 1) + " "+ "position");
            checkMathExpression = "no";
        }

        if (counter > 0) {
            System.out.println ("The mathexpression has " + counter + " redundant left brackets");
            checkMathExpression = "no";
        }
        return checkMathExpression;
    }
    }


