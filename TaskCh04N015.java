package my_pack;

import java.util.Scanner;

/*
  Известны год и номер месяца рождения человека, а также год и номер месяца
  сегодняшнего дня (январь — 1 и т. д.). Определить возраст человека (число
  полных лет). В случае совпадения указанных номеров месяцев считать, что
   прошел полный год.
 */

public class TaskCh04N015 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter year and month of birthday");
        int yearBirthday = scanner.nextInt();
        int monthBirthday = scanner.nextInt();
        System.out.println("Please enter current year and month");
        int yearCurrent = scanner.nextInt();
        int monthCurrent = scanner.nextInt();
        int age =calculateAge(yearBirthday, monthBirthday, yearCurrent, monthCurrent);
        printResult(age);
    }

    public  static int calculateAge(int yearBirthday, int monthBirthday, int yearCurrent, int monthCurrent) {

        if (monthCurrent >= monthBirthday) {
            return yearCurrent - yearBirthday;
        } else
            return yearCurrent - yearBirthday - 1;
    }
    private static void printResult(int age) {

     System.out.println("Age = " + age);
    }
}
