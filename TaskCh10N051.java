package my_pack;

/*
Определить результат выполнения следующих рекурсивных процедур при n=5
 */

public class TaskCh10N051 {

    public static void main(String[] args) {
        int N = 5;

        System.out.println("The result of operation1:");
        operation1(N);
        System.out.println("The result of operation2:");
        operation2(N);
        System.out.println("The result of operation3:");
        operation3(N);
    }

    private static void operation1(int N) {
        if (N > 0) {
            System.out.println(N);
            operation1(N - 1);
        }
    }

    private static void operation2(int N) {
        if (N > 0) {
            operation2(N - 1);
            System.out.println(N);
        }
    }

    private static void operation3(int N) {
        if (N > 0) {
            System.out.println(N);
            operation3(N - 1);
            System.out.println(N);
        }
    }

}
