package my_pack;

import java.util.Arrays;

/*
Удалить из массива все повторяющиеся элементы, оставив их первые вхождения, т. е. в массиве должны остаться только
различные элементы.
 */

public class TaskCh11N158 {

    public static void main(String[] args) {
        int[] array = {55, 26, 17, 8, 8, 68};
        int[] newArray = removeDuplicateElements(array);
        System.out.println("The array without duplicates is = " + Arrays.toString(newArray));
    }

    static int[] removeDuplicateElements(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int firstMember = array[i];
            int j = i + 1;
            while (j < array.length) {
                if (array[j] == firstMember) {
                    System.arraycopy(array, j + 1, array, j, array.length - 1 - j);
                    array[array.length - 1] = 0;
                }
                j++;
            }
        }
        return array;
    }
}


