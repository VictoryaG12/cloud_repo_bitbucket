package my_pack;

import java.util.Scanner;
import java.util.Arrays;
/*
Заполнить двумерный массив размером 7*7 так, как показано на рис. 12.1.
solve for N*N, N is odd
 */

public class TaskCh12N023 {

    public static void main(String[] args) {
            System.out.println("Please enter a size of the array (odd number)");
            Scanner scanner = new Scanner(System.in);
            int size = scanner.nextInt();
            System.out.println(" The array A");
            for (int[] i : arrayA(size)) {
                System.out.println(Arrays.toString(i));
            }
            System.out.println(" The array B");
            for (int[] i : arrayB(size)) {
                System.out.println(Arrays.toString(i));
            }
            System.out.println(" The array C");
            for (int[] i : arrayC(size)) {
                System.out.println(Arrays.toString(i));
            }
        }

        private static int[][] arrayA ( int size){
            int[][] array = new int[size][size];
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    if (i == j || j + i == size - 1) {
                        array[i][j] = 1;
                    }
                }
            }
            return array;
        }

        private static int[][] arrayB ( int size){
            int[][] array = new int[size][size];
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    if (i == j || j + i == size - 1 || i == size/2 || j == size/2) {
                        array[i][j] = 1;
                    }
                }
            }
            return array;
        }

       private static int[][] arrayC (int size){
            int[][] array = new int[size][size];
            for (int i = 0; i < size; i++) {
                for (int j = i; j < size - i; j++) {
                    array[i][j] = 1;
                    array[size - i - 1][j] = 1;
                }
            }
            return array;
        }
    }


