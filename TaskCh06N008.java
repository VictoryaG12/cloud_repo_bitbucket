package my_pack;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import java.util.Scanner;

/*
Дано число n. Из чисел 1, 4, 9, 16, 25, ... напечатать те, которые не превышают n.
 */
public class TaskCh06N008 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter value of a variable N: ");
        int variableN = scanner.nextInt();
        for (int a : getNumbers(variableN)) {
            System.out.println(a);
        }

    }

    public static int[] getNumbers(int variableN) {
        int[] output;
        output = new int[(int) sqrt(variableN)];
        for (int i = 0; i < output.length; i++)
            output[i] = (int) pow((i + 1), 2);

        return output;
    }
}