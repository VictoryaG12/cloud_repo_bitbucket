package my_pack;

import java.util.Scanner;
/*
Составить программу, которая ведет учет очков, набранных каждой командой при игре в баскетбол. Количество очков,
полученных командами в ходе игры, может быть равно 1, 2 или 3. После любого изменения счет выводить на экран.
После окончания игры выдать итоговое сообщение и указать номер команды-победительницы.
Console input must be organized as Enter team #1: Enter team #2: Enter team to score (1 or 2 or 0 to finish game):
Enter score (1 or 2 or 3): Game class must have such methods (not limited to).
 */


public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

class Game {
    private String teamNumber1;
    private String teamNumber2;
    private int scoreTeam1;
    private int scoreTeam2;

    Game() {
    }

    void play() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter team #1");
        teamNumber1 = in.next();
        System.out.println("Enter team #2");
        teamNumber2 = in.next();
        while (true) {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
            int selection = in.nextInt();
            if (selection == 0) {
                System.out.println(result());
                in.close();
                break;
            }
            System.out.println("Enter score (1 or 2 or 3):");
            int score = in.nextInt();
            if (selection == 1) {
                scoreTeam1 += score;
                System.out.println(score());
            } else if (selection == 2) {
                scoreTeam2 += score;
                System.out.println(score());
            }
        }
    }

     private String score() {
        return teamNumber1 + ":" + scoreTeam1 + " " + teamNumber2 + ":" + scoreTeam2;
    }

    public String result() {
        if (scoreTeam1 >= scoreTeam2) {
            return "Winner " + teamNumber1 + "=" + scoreTeam1 + " : " + "Looser " + teamNumber2 + "=" + scoreTeam2;
        } else {
            return "Winner " + teamNumber2+ "=" + scoreTeam2 + " : " + "Looser " + teamNumber1 + "=" + scoreTeam1;
        }
    }

    public void setTeamNumber1(String teamNumber1) {
        this.teamNumber1 = teamNumber1;
    }

    public void setTeamNumber2(String teamNumber2) {
        this.teamNumber2 = teamNumber2;
    }

    public void setScoreTeamNumber1(int scoreTeam1) {
        this.scoreTeam1 = scoreTeam1;
    }

    public void setTeamNumber2(int scoreTeam2) {
        this.scoreTeam2 = scoreTeam2;
    }
}