package my_pack;

import java.util.Scanner;
/*
Дано слово. Вывести на экран его k-й символ.
 */

public class TaskCh09N015 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the word:");
        String inputWord = scanner.next();
        System.out.println("Please enter number of a letter of your choice");
        int characterK = scanner.nextInt();
        System.out.println(getSymbolOfK(characterK, inputWord));

    }

    private static char getSymbolOfK (int characterK, String inputWord) {
        return inputWord.charAt(characterK);
    }
}
