package my_pack;
/*
Main class
*/
public class Interview2 {

    /*
    *Main method
    */
    public static void main(String [] args){
        
        //initializing the array of the candidates
        candidate candidates[] = new candidate[10];

        //filling the array
        fillArray(candidates);

        //initializing the interviewer
        interviewer employee = new interviewer();

        //looping through the 10 candidates
        for(int i = 0;i<10;i++){

            //interviewer saying
            employee.speak();

            //candidate saying
            candidates[i].speak();
            
        }  
        
    }

    /*
    *method to fill the candidate array
    */
    private static void fillArray(candidate[] candidates){

        candidates[0] = new graduate("Mike");
        candidates[1] = new selfLearner("Peter");
        candidates[2] = new graduate("Sasha");
        candidates[3] = new selfLearner("Robert");
        candidates[4] = new graduate("George");
        candidates[5] = new selfLearner("Ken");
        candidates[6] = new graduate("Steve");
        candidates[7] = new selfLearner("Slava");
        candidates[8] = new graduate("Denis");
        candidates[9] = new selfLearner("Phillip");
        
    }
    
}

/*
 Class  candidate.java
 */

/*
Abstract class implemented for the use of both graduate and self-Learner
*/

abstract class candidate implements human{
    
    //Name of the candidate (Graduate or the self-Learner)
    public String name;
    
    //Constructor with initialising name
    public candidate(String name){
        this.name = name;
    }


    /*
    Methods implemented by human interface
     */

    public String salute() {
        throw new UnsupportedOperationException("Not supported yet.");

    }
    

    public String clause() {
        throw new UnsupportedOperationException("Not supported yet.");

    }
    

    public void speak() {
        throw new UnsupportedOperationException("Not supported yet.");

    }

}

/*
  class        graduate.java
 */

/*
 Object class for the graduate
*/
class graduate extends candidate{
    
    //constructor to initialize  the name
    public graduate(String name){
        super(name);
    }

    /*
    *Methods implemented by human interface
    */
    //salute with the name

    public String salute() {
        StringBuilder greet = new StringBuilder();
        greet.append("Hi! my name is ");
        greet.append(name);
        return greet.toString();
    }
    
    //specific clause for the graduate
    public String clause() {
        String say = "\nI passed successfully getJavaJob exams and code reviews";
        return say;
    }
    
    //printing both clauses in the console

    public void speak() {
        System.out.println(salute() + clause());
    }
    
}

/*
 interface for all the characters involved in the interview
*/
interface human {

    //salute clause
    public String salute();

    //other remaining clause
    public String clause();

    //printing in the console
    public void speak();
}

/*
  Class        interviewer.java
 */

/*
 Object class for the interviewer
*/
class interviewer implements human{

    /*
    Methods implemented by human interface
    */

    //salute

    public String salute() {
        String greet = "Hi! ";
        return greet;
    }

    //next clause
    public String clause() {
        String say = "Introduce yourself and describe your java experience please";
        return say;
    }

    //Printing saying in the console

    public void speak() {
        System.out.println(salute() + clause());
    }
    
}

/*
 * class        selfLearner.java
*/

/*
Object class for the self learner
*/
 class selfLearner extends candidate{
    

    //constructor to initialize the name
    public selfLearner(String name) {
        super(name);
    }

    /*
    Methods implemented by human interface
    */
    //salute with the name

    public String salute() {
        StringBuilder greet = new StringBuilder();
        greet.append("Hi! my name is ");
        greet.append(name);
        return greet.toString();
    }
    
    //specific clause for the self learner

    public String clause() {
        String say = "\nI have been learning java by myself, nobody examined"
                + " "+ "how thorough is my knowledge and how good is my code.";
        return say;
    }
    
    //printing  both  clauses in the console

    public void speak() {
        System.out.println(salute() + clause());
    }
    
}
