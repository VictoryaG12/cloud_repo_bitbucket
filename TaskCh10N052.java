package my_pack;

import java.util.Scanner;
/*
Написать рекурсивную процедуру для вывода на экран цифр натурального
числа в обратном порядке.
 */

 public class TaskCh10N052 {
     public static void main(String args[]) {
         int inputNumber = 0;
         System.out.println("Please input your number: ");
         Scanner in = new Scanner(System.in);
         inputNumber = in.nextInt();
         System.out.print("The reversed input number is:");
         findReversedNumber(inputNumber);
         System.out.println();
     }

     private static void findReversedNumber(int number) {
         if (number < 10) {
             System.out.println(number);

         } else {
             System.out.print(number % 10);
             findReversedNumber(number / 10);
         }
     }
 }


