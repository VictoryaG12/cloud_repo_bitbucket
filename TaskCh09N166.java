package my_pack;


/*
 * Дано предложение. Поменять местами его первое и последнее слово.
 * String.split method.
 */
public class TaskCh09N166 {

  public static void main(String[] args) {
  String oneSentence = switchFirstAndLastWord("I like to program");
  printResult(oneSentence);
}

 private static String switchFirstAndLastWord(String swapSentence) {
   StringBuilder word = new StringBuilder();
   String[] splitSentence = swapSentence.split(" ");

     word.append(splitSentence[splitSentence.length - 1]).append(" ");
     for (int i = 1; i < splitSentence.length - 1; i++) {
       word.append(splitSentence[i]).append(" ");
     }
     word.append(splitSentence[0]);

     return word.toString();
   }

  private static void printResult(String result) {
    System.out.println(result);
  }
}




