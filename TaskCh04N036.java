package my_pack;

import java.util.Scanner;

/*В начале каждого часа в течение трех минут горит зеленый сигнал, затем в течение двух минут — красный, в
  течение трех минут — опять зеленый и т. д. Дано вещественное число t, означающее время в минутах, прошедшее с начала
  очередного часа. Определить, сигнал какого цвета горит для пешеходов в этот момент.
 */

public class TaskCh04N036 {

    public static void main(String[] args) {
        System.out.println("Please input time in minutes");
        Scanner scanner = new Scanner(System.in);
        int time = scanner.nextInt();
        String colour = trafficLightColour(time);
        printResult(colour);
    }

     public static String trafficLightColour(int t) {
        return t % 5 >= 3 ? "red" : "green";
    }

    private static void printResult(String colour) {
        System.out.println("The color of traffic light is " + colour);
    }
}



