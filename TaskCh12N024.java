package my_pack;

import java.util.Arrays;
import java.util.Scanner;
/*
Заполнить массив размером 6*6 так, как показано на рис. 12.2.
solve for N*N, in b solve with arithmetic expression for a [i][j]
 */

public class TaskCh12N024 {
    public static void main(String[] args) {

        System.out.println("Please input size array:");
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        System.out.println(" The array A");
        for (int[] i : arrayA(N)) {
            System.out.println(Arrays.toString(i));
        }
        System.out.println(" The array B");
        for (int[] i : arrayB(N)) {
            System.out.println(Arrays.toString(i));
        }
    }


   private static int[][] arrayA(int N) {
        int[][] newArrayA = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (i == 0 || j == 0) {
                    newArrayA[i][j] = 1;
                } else {
                    newArrayA[i][j] = newArrayA[i - 1][j] + newArrayA[i][j - 1];
                }
            }
        }
        return newArrayA;
    }

   private static int[][] arrayB(int N) {
        int[][] newArrayB = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (i == 0) {
                    newArrayB[i][j] = j + 1;
                } else if (j < N - i) {
                    newArrayB[i][j] = newArrayB[i - 1][j + 1];
                } else {
                    newArrayB[i][j] = j - (N - i) + 1;
                }
            }
        }
        return newArrayB;
    }

}


