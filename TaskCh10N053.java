package my_pack;

/*
Написать рекурсивную процедуру для ввода с клавиатуры последовательности чисел и вывода ее на экран в обратном порядке
(окончание последовательности — при вводе нуля).
Replace input with array.
 */
public class TaskCh10N053 {

    public static void main(String[] args) {
        int[] array = {5,6,7,8,9,0};
        reverseArray(array,0,array.length-1);
        for (int i : array) {
            System.out.print(i);
        }
    }

    private static void reverseArray(int[] array, int start, int end) {
        if (start < end) {
            int t = array[start];
            array[start] = array[end];
            array[end] = t;
            reverseArray(array, start + 1, end - 1);
        }
    }
}

