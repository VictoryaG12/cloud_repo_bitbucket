package my_pack;

import java.util.Scanner;
/*
Определить:
а) на каком расстоянии от дома будет находиться мужчина после 100-го этапа;
б) какой общий путь он при этом пройдет.
 */

public class TaskCh05N038 {

    public static void main(String[] args) {
        System.out.println("Please input number phase");
        Scanner scanner = new Scanner(System.in);
        int phase = scanner.nextInt();
        System.out.println(getWalkFromHome(phase));
        System.out.println(getTotalDistanceWalk(phase));
    }

    private static double getWalkFromHome(int x) {
        double lengthFromHome = 0;
        for (int i = 1; i <= x; i += 2) {
            lengthFromHome += 1 / i;
        }
        for (int i = 2; i <= x; i += 2) {
            lengthFromHome -= 1 / i;
        }
        return lengthFromHome;

    }
    private static double getTotalDistanceWalk(int x) {
        double totalDistanceWalk = 0;
        for (int i = 1; i <= x; i++)
            totalDistanceWalk += 1 / i;
        return totalDistanceWalk;
    }

}