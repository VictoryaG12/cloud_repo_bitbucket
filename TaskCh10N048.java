package my_pack;


/*
Написать рекурсивную функцию для вычисления максимального элемента массива из n элементов.
 */

import static java.lang.Math.max;

public class TaskCh10N048 {

    public static void main(String[] args) {
        int[] array = {250, 500, 300, 400};

        System.out.println("The max element " + findMaxElement(array,  3));
    }

    private static int findMaxElement(int[] array, int index) {
        if (index > 0) {
            return max(array [index], findMaxElement(array, index - 1));
        } else {
            return array[0];
        }
    }
}

//    private static int getMaxValue(int[] array, int number) {
//        return number > 0 ? max(array[number - 1], getMaxValue(array, number - 1)) : array[0];
//    }
//}

