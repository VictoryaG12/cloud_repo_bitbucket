package my_pack;

import static java.lang.Math.round;

/*
В области 12 районов. Известны количество жителей (в тысячах человек)
и площадь (в км2) каждого района. Определить среднюю плотность населения
по области в целом.
 */

public class TaskCh05N064 {

  public static void main(String[] args) {
    double[] numberOfInhabitants = new double[]{1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,
      11000, 12000};
    double[] areaOfDistrict = new double[]{100.1, 200.2, 300.3, 400.4, 500.5, 600.6, 700.7, 800, 900, 100,
      800.1, 55.6};
    double populationDensity = findDensityOfPopulation(numberOfInhabitants, areaOfDistrict);
    printResult(populationDensity);
  }

  private static double findDensityOfPopulation(double[] numberOfInhabitants, double[] areaOfDistrict) {
    double output = 0;

    for (int k = 0; k < 12; k++) {
      output += numberOfInhabitants[k] / areaOfDistrict[k];

    }
    return round(output / 12);
  }

  private static void printResult(double output) {
    System.out.println("The average population density is: " + output);
  }
}


