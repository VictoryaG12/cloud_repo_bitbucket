package my_pack;

import java.util.Arrays;

/*
Дан массив. Переписать его элементы в другой массив такого же размера следующим образом: сначала должны идти все
отрицательные элементы, а затем все остальные. Использовать только один проход по исходному массиву.
 */

public class TaskCh11N245 {

    public static void main(String[] args) {
        int[] array = {10, -25, 36, 48, -2, -30};
        int[] newArray = NegativeElementsFirst(array);
        System.out.println(Arrays.toString(newArray));

    }

   private static int[] NegativeElementsFirst(int[] originalArray) {
        int[] resultArray = new int[originalArray.length];
        int positiveNumberIndex = originalArray.length - 1;
        int negativeNumberIndex = 0;

        for (int number : originalArray) {
            if (number < 0) {
                resultArray[negativeNumberIndex] = number;
                negativeNumberIndex++;
            } else {
                resultArray[positiveNumberIndex] = number;
                positiveNumberIndex--;
            }
        }
        return resultArray;
    }
}