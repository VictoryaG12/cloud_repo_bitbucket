package my_pack;

/*
Написать рекурсивную функцию для вычисления значения так называемой
функции Аккермана для неотрицательных чисел n и m.
 Найти значение функции Аккермана для n =1, m = 3.
 */

public class TaskCh10N050 {

    public static void main(String[] args) {

        int number = findAckermannFunction(1, 3);
        System.out.println("The quantity of the Ackermann function = " + number);

    }

    private static int findAckermannFunction(int n, int m) {
        if (n == 0) {
            return m + 1;
        } else if (m == 0 && n > 0) {
            return findAckermannFunction(n - 1, 1);
        } else {
            return findAckermannFunction(n - 1, findAckermannFunction (n, m - 1));
        }
    }
}

