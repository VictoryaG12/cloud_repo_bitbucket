package my_pack;

import java.util.Scanner;
/*
Написать рекурсивную функцию для расчета степени n вещественного числа a (n — натуральное число).
 */

public class TaskCh10N042 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the number");
        int numberA = scanner.nextInt();
        System.out.println("Please enter a power of number");
        int power = scanner.nextInt();
        System.out.println("\n" + +numberA + "^" + power + " = " + findPowerOfNumber(numberA, power));
    }

    private static int findPowerOfNumber(int numberA, int power) {
        if (power == 0)
            return 1;
        else if (power == 1)
            return numberA;
        else
            return numberA * findPowerOfNumber(numberA, power - 1);
    }
}


