package my_pack;

import java.util.Scanner;

/*
Дано слово, состоящее из четного числа букв. Вывести на экран его первую половину, не используя оператор цикла.
 */

public class TaskCh09N022 {
    public static void main(String[] args) {
        System.out.println("Please input a word:");
        Scanner scanner = new Scanner(System.in);
        String SingleWord = scanner.next();
        System.out.println(findFirstPartWord(SingleWord));
    }

    private static String findFirstPartWord(String SingleWord) {
        int wordLength = SingleWord.length();
        return SingleWord.substring(0, wordLength / 2);

    }
}