package my_pack;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/*
Известна информация о 20 сотрудниках фирмы: фамилия, имя, отчество, адрес и дата поступления на работу (месяц, год).
Напечатать фамилию, имя, отчество и адрес сотрудников, которые на сегодняшний день проработали в фирме не менее трех
лет. День месяца не учитывать (при совпадении месяца поступления и месяца сегодняшнего дня считать, что прошел полный
 год).
 */


public class TaskCh13N012 {
  public static void main(String[] args) {
    Database base = new Database();
    base.printEmployee(base.EmployeeWorkYears(3));
  }
}

class Employee {
  private String lastName;
  private String firstName;
  private String middleName;
  private String address;
  private int monthStart;
  private int yearStart;

  public Employee(String lastName, String firstName, String middleName, String address, int monthStart,
                  int yearStart) {
    this.lastName = lastName;
    this.firstName = firstName;
    this.middleName = middleName;
    this.address = address;
    this.monthStart = monthStart;
    this.yearStart = yearStart;
  }

  public Employee(String lastName, String firstName, String address, int monthStart, int yearStart) {
    this(lastName, firstName, "", address, monthStart, yearStart);
  }


  public int getMonthStart() {
    return monthStart;
  }

  public int getYearStart() {
    return yearStart;
  }


  public String toString() {
    return "Employee{" +
      "lastName=" + lastName + " " + ", firstName=" + firstName + " " + ", middleName=" + middleName + " " +
      ", address=" + address + " " + ", monthStart=" + monthStart + ", yearStart=" + yearStart +
      "}";
  }
}

class Database {
  private List<Employee> employees = new ArrayList<>();
  private int yearToday;
  private int monthToday;

  public Database() {
    createDataBase();
  }

  private void createDataBase() {
    employees.add(new Employee("Kazakov", "Ivan", "Petrovich", "Ufa",
      6, 2014));
    employees.add(new Employee("Korolev", "Petr", "Vladimirovich", "Moscow",
      8, 2016));
    employees.add(new Employee("Ovsov", "Stepan", "Viktorovich", "Samara",
      7, 2015));
    employees.add(new Employee("Gagarin", "Yuriy","Sergeevich", "Moscow",
      5, 2015));
    employees.add(new Employee("Sidorov", "Ivan","Vasilevich", "Chelyabinsk",
      4, 2017));
    employees.add(new Employee("Polyakov", "Petr", "Petrovich", "Tyumen",
      1, 2014));
    employees.add(new Employee("Kotikov", "Denis", "Evgenievich",
      "Kazan", 10, 2015));
    employees.add(new Employee("Semenova", "Irina", "Leonidovna","Moscow",
      6, 2014));
    employees.add(new Employee("Kukushkina", "Svetlana", "Valerievna",
      "Ekaterinburg", 2, 2016));
    employees.add(new Employee("Semenov", "Semen", "Semenovich", "Moscow",
      6, 2017));
    employees.add(new Employee("Kononov", "Sergey", "Alexeyevich",
      "Samara", 2, 2016));
    employees.add(new Employee("Kotova", "Larisa", "Ivanovna","Moscow",
      3, 2013));
    employees.add(new Employee("Plakhova", "Nadezhda", "Yurevna", "Kiev",
      2, 2012));
    employees.add(new Employee("Deryugina", "Ekaterina", "Vladimirovna", "Krasnodar",
      7, 2017));
    employees.add(new Employee("Simina", "Natalia", "Anatolievna",
      "Saint Petersburg", 5, 2016));
    employees.add(new Employee("Vedernikova", "Anna", "Stanislavovna", "Moscow",
      9, 2015));
    employees.add(new Employee("Fedorova", "Yuliya", "Valeryevna", "Omsk",
      1, 2016));
    employees.add(new Employee("Barinova", "Elena", "Nikolaevna",
      "Moscow", 7, 2015));
    employees.add(new Employee("Kholina", "Yulia", "Valerievna",
      "Moscow", 6, 2015));
    employees.add(new Employee("Voronina", "Svetlana", "Anatolievna",
      "Moscow", 11, 2015));
  }

 public void printEmployee(List<Employee> employees) {
    for (Employee list : employees) {
      System.out.println(list.toString());
    }
    System.out.println();
  }

 public int WorkExperience(Employee employee) {
    if (monthToday >= employee.getMonthStart()) {
      return yearToday - employee.getYearStart();
    } else {
      return yearToday - employee.getYearStart() - 1;
    }
  }

  public List<Employee> EmployeeWorkYears (int Work3Years) {
    GregorianCalendar today = new GregorianCalendar();
    yearToday = today.get(Calendar.YEAR);
    monthToday = today.get(Calendar.MONTH);
    List<Employee> employeesAtLeastYears = new ArrayList<>();
    for (Employee list : employees) {
      int yearsOfWork = WorkExperience(list);
      if (Work3Years <= yearsOfWork) {
        employeesAtLeastYears.add(list);
      }
    }
    return employeesAtLeastYears;
  }
}
