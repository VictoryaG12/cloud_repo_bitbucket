package my_pack;
/*
Object class for the graduate
 */

import my_pack.candidate;

public class graduate extends candidate {
    
    //constructor to initialize  the name

    public graduate(String name){
        super(name);
    }
    
    /*
    Methods implemented by human interface
     */

    //salute with the name

    public String salute() {
        StringBuilder greet = new StringBuilder();
        greet.append("Hi! my name is ");
        greet.append(name);
        return greet.toString();
    }
    
    //specific clause for the graduate

    public String clause() {
        String say = "\nI passed successfully getJavaJob exams and code reviews";
        return say;
    }
    
    //printing the both the clauses in the console

    public void speak() {
        System.out.println(salute() + clause());
    }
    
}
