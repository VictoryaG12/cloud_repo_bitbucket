package my_pack;

import java.util.Scanner;
/*
Даны первый член и знаменатель геометрической прогрессии. Написать рекурсивную функцию:
а) нахождения n-го члена прогрессии;
б) нахождения суммы n первых членов прогрессии.
 */

public class TaskCh10N046 {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Please enter the first element of geometric progression");
    int firstElement = scanner.nextInt();
    System.out.println("Please enter denominator of geometric progression");
    int denominator = scanner.nextInt();
    System.out.println("Please enter an index of element N");
    int indexOfNElement = scanner.nextInt();
    int elementN = findNElement(firstElement, denominator, indexOfNElement);
    int SumofElements = findSumOfElements(firstElement, denominator, indexOfNElement);
    System.out.println("The N element =" + elementN);
    System.out.println("The sum of first elements =" + SumofElements);

  }

  private static int findNElement(int firstElement, int denominator, int indexOfNElement) {
    if (indexOfNElement == 1) {
      return firstElement;
    } else {
      return findNElement(denominator * firstElement, denominator, indexOfNElement - 1);
    }
  }

  private static int findSumOfElements(int firstElement, int denominator, int indexOfNElement) {
    if (indexOfNElement == 0) {
      return 0;
    } else {
      return firstElement + findSumOfElements(denominator * firstElement, denominator,
        indexOfNElement - 1);
    }
  }
}
