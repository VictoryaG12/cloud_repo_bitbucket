package my_pack;

import java.util.Scanner;

/*
 * Даны два целых числа a и b. Если a делится на b или b делится на a, то вывести 1,
 * иначе — любое другое число. Условные операторы и операторы цикла не использовать.
 */

    public class TaskCh02N043 {
    public static void main(String[] args) {
        System.out.println("Enter the number a and b on the new line");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println(getResultNumber(a, b));
    }

     private static int getResultNumber (int a, int b) {
        return (a % b) * (b % a) + 1;
    }
}

