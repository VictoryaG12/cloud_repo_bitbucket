package my_pack;


import java.util.Scanner;
/*
Заполнить двумерный массив размером 5 5 так, как представлено на рис. 12.4.
solve for array N*N, where N%2=1
 */

public class TaskCh12N028 {

  public static void main(String[] args) {
      System.out.println("Please enter an odd array size:");
      Scanner scanner = new Scanner(System.in);
      int N = scanner.nextInt();
      if (N % 2 == 1) {
        printArray(newArray(N));
      } else {
        System.out.println("Wrong input, you entered even number");
      }
    }

   private static int[][] newArray(int N) {
      int[][] newArray = new int[N][N];
      int number = 1;
      int column1 = 0;
      int column2 = N - 1;
      int row1 = 0;
      int row2 = N - 1;
      while (number <= newArray.length * newArray[1].length) {
        for (int i = column1; i <= column2; i++) {
          newArray[row1][i] = number;
          number++;
        }
        for (int j = row1 + 1; j <= row2; j++) {
          newArray[j][column2] = number;
          number++;
        }
        for (int i = column2 - 1; i >= column1; i--) {
          newArray[row2][i] = number;
          number++;
        }
        for (int j = row2 - 1; j >= row1 + 1; j--) {
          newArray[j][column1] = number;
          number++;
        }
        column1++;
        column2--;
        row1++;
        row2--;
      }
      return newArray;
    }

  private static void printArray(int[][] array) {
    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array[i].length; j++) {
        System.out.printf("%4d", array[i][j]);
      }
      System.out.println();
    }
    System.out.println();
  }
}