package my_pack;

import java.util.Scanner;

/*
Составить программу, которая по заданному номеру года нашей эры n печатает его название по описанному
календарю в виде: "Крыса, Зеленый".
n>=1984
 */

public class TaskCh04N115 {

    public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.println("Please enter the year");
      int inputYear = scanner.nextInt();
      String animal = getNameOfAnimal(inputYear);
      String colour = getColourOfElement(inputYear);
      printResult(animal, colour);
    }

  static String getNameOfAnimal(int inputYear) {
    String[] arrayOfAnimals = {"Rat", "Cow", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Sheep", "Monkey",
      "Rooster", "Dog", "Pig"};

    return arrayOfAnimals[(inputYear - 4) % 12];
  }

  static String getColourOfElement(int inputYear) {
    String[] arrayOfColors = {"Green", "Green", "Red", "Red", "Yellow", "Yellow", "White", "White", "Black", "Black"};
    return arrayOfColors[(inputYear - 4) % 10];
  }

  private static void printResult(String animal, String colour) {

    System.out.println(animal + ", " + colour);
  }

}