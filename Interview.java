package my_pack;

/*
main class
 */
public class Interview {


    /*
    Main method
     */

    public static void main(String [] args){
        
        //Initializing the array of the candidates
        candidate candidates[] = new candidate[10];

        //filling the array
        fillArray(candidates);

        //initializing the interviewer
        interviewer employee = new interviewer();

        //looping through the 10 candidates
        for(int i = 0;i<10;i++){

            //interviewer saying
            employee.speak();

            //candidate saying
            candidates[i].speak();
            
        }  
        
    }

    /*
    method to fill the candidate array
     */

    public static void fillArray(candidate[] candidates){
        
        candidates[0] = new graduate("Mike");
        candidates[1] = new selfLearner("Peter");
        candidates[2] = new graduate("Sasha");
        candidates[3] = new selfLearner("Robert");
        candidates[4] = new graduate("George");
        candidates[5] = new selfLearner("Ken");
        candidates[6] = new graduate("Steve");
        candidates[7] = new selfLearner("Slava");
        candidates[8] = new graduate("Denis");
        candidates[9] = new selfLearner("Phillip");
        
    }
    
}
