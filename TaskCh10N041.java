package my_pack;

import java.util.Scanner;

/*
Написать рекурсивную функцию для вычисления факториала натурального числа n.
 */

public class TaskCh10N041 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter number N");
        int numberN = scanner.nextInt();
        int factorial = findFactorial (numberN);
      System.out.println("Factorial of entered number is: "+ factorial);
    }


    private static int findFactorial(int N) {
      int output;
      if (N ==1) {
        return 1;
      }
      else
        output=findFactorial (N-1) *N;
      return output;

    }
}
