package my_pack;

import java.util.Scanner;
/*
Даны первый член и разность арифметической прогрессии. Написать рекурсивную функцию для нахождения:
а) n-го члена прогрессии;
б) суммы n первых членов прогрессии.
 */

public class TaskCh10N045 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the first element of arithmetic progression");
        int firstElement = scanner.nextInt();
        System.out.println("Please enter difference of arithmetic progression");
        int difference = scanner.nextInt();
        System.out.println("Please enter an index of element N");
        int indexOfNElement = scanner.nextInt();
        int elementN=findNElement(firstElement, difference, indexOfNElement);
        int SumofElements = findSumOfElements(firstElement, difference, indexOfNElement);
        System.out.println("The N element is:=" + elementN);
        System.out.println("The sum of first elements is:=" + SumofElements);

    }

    private static int findNElement(int firstElement, int difference, int indexOfNElement) {
      if (indexOfNElement==1) {
        return firstElement;
      }
      return firstElement* difference + findNElement(firstElement, difference, indexOfNElement - 1);

    }

    private static int findSumOfElements(int firstElement, int difference, int indexOfNElement) {

      if (indexOfNElement==0){
        return 0;
      }

      return firstElement + findSumOfElements (firstElement + difference, difference,
        indexOfNElement-1 );
    }
}