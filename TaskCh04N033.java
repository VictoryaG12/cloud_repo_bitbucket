package my_pack;

/*
  Дано натуральное число.
  а) Верно ли, что оно заканчивается четной цифрой?
  б) Верно ли, что оно заканчивается нечетной цифрой?
 */

public class TaskCh04N033 {

    public static void main(String[] args) {
        int inputNumber = 252;
        boolean evenNumber = isLastDigitEven(inputNumber);
        boolean oddNumber = isLastDigitOdd(inputNumber);
        printResult(evenNumber, oddNumber);
    }

    static boolean isLastDigitEven(int inputNumber) {
        return inputNumber % 2 == 0;
    }

    static boolean isLastDigitOdd(int inputNumber) {
        return inputNumber % 2 != 0;
    }

    private static void printResult(boolean evenNumber, boolean oddNumber) {
        System.out.println("Does the number end with an even number? The answer: " + evenNumber);
        System.out.println("Does the number end with an odd number?  The answer: " + oddNumber);
    }
}



