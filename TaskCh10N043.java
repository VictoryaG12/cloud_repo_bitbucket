package my_pack;

import java.util.Scanner;
/*
Написать рекурсивную функцию:
а) вычисления суммы цифр натурального числа;
б) вычисления количества цифр натурального числа.
 */

public class TaskCh10N043 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number N");
        int numberN = scanner.nextInt();
        int s = sumOfDigits(numberN);
        int n = numberOfDigits(numberN);
        System.out.println("The sum of digits is:=" + s);
        System.out.println("The number of digits is:=" + n);
    }

    private static int sumOfDigits(int number) {
        int digitSum;
        if (number == 0) {
            return 0;
        } else {
            digitSum = sumOfDigits (number/10)+ number % 10;
            return digitSum;
        }
    }

    private static int numberOfDigits(int number) {
        int amountOfNumbers;
        if (number == 0) {
            return 0;
        }
            else {
            amountOfNumbers=numberOfDigits(number / 10) + 1;
        }
        return amountOfNumbers;
    }
}



