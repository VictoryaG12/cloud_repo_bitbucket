package my_pack;

import java.util.Scanner;

/*
 1=< k <=365
 test1: in 5 out workday;
 test2: in 7 out weekend;
 */

public class TaskCh04N067 {

    public static void main(String[] args) {
        System.out.println("Please input day from 1 to 365");
        Scanner scanner = new Scanner(System.in);
        int day = scanner.nextInt();
        System.out.println(getDayOfWeek(day));
    }

    public static String getDayOfWeek(int k) {
        int day = k % 7;
        return day == 0 || day == 6 ? "Weekend" : "Workday";
    }
}