package my_pack;

import java.util.Scanner;

/*
Написать рекурсивную функцию, определяющую, является ли заданное натуральное число простым (простым называется
натуральное число, большее 1, не имеющее других делителей, кроме единицы и самого себя).
optimize number of divisor checks
 */

public class TaskCh10N056 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the number");
        int N = scanner.nextInt();
        System.out.println(isNumberPrime(N));
    }

   private static boolean isNumberPrime (int number) {
       if ((number % 2) == 0 && number != 2) {
           return false;
       } else {
           for (int i = 3; i * i < number; i++) {
               if (number % i == 0)
                   return false;
           }
           return true;
       }
   }
}


