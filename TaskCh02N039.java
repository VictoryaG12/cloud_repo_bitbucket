package my_pack;

/*
 * Даны целые числа h, m, s (0 < h ≤ 23, 0 ≤ m ≤ 59, 0 ≤ s ≤ 59), указывающие
 * момент времени: "h часов, m минут, s секунд". Определить угол (в градусах)
 * между положением часовой стрелки в начале суток и в указанный момент времени.
 */

public class TaskCh02N039 {
    public static void main(String[] args) {
      double angle = searchAngle(18, 0, 0);
      printAngle(angle);
    }

    static double searchAngle(int hours, int minutes, int seconds) {
      if (hours > 12) {
        hours -= 12;
      }

      int time = 0;
      time += seconds;
      time += 60 * minutes;
      time += 60 * 60 * hours;
      double angle = (double) 360 / (12 * 60 * 60);
      return angle * time;
    }

    private static void printAngle(double result) {
      System.out.println("The angle is " + result + " degrees");
    }
  }

