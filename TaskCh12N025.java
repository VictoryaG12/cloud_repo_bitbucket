package my_pack;

import java.util.Arrays;
/*
Заполнить двумерный массив так, как представлено на рис. 12.3.
 */

public class TaskCh12N025 {

    public static void main(String[] args) {
        int[][] arrayA = arrayA(12, 10);
        printArray(arrayA);
        int[][] arrayB = arrayB (12, 10);
        printArray(arrayB);
        int[][] arrayV = arrayV(12, 10);
        printArray(arrayV);
        int[][] arrayG = arrayG(12, 10);
        printArray(arrayG);
        int[][] arrayD = arrayD(10, 12);
        printArray(arrayD);
        int[][] arrayE = arrayE(12, 10);
        printArray(arrayE);
        int[][] arrayJ = arrayJ(12, 10);
        printArray(arrayJ);
        int[][] arrayZ = arrayZ(12, 10);
        printArray(arrayZ);
        int[][] arrayI = arrayI(12, 10);
        printArray(arrayI);
        int[][] arrayK = arrayK(12, 10);
        printArray(arrayK);
        int[][] arrayL = arrayL(12, 10);
        printArray(arrayL);
        int[][] arrayM = arrayM(12, 10);
        printArray(arrayM);
        int[][] arrayN = arrayN(12, 10);
        printArray(arrayN);
        int[][] arrayO = arrayO(12, 10);
        printArray(arrayO);
        int[][] arrayP = arrayP(12, 10);
        printArray(arrayP);
        int[][] arrayR = arrayR(12, 10);
        printArray(arrayR);
    }

    private static void printArray(int[][] array) {
        for (int i[] : array) {
            System.out.println(Arrays.toString(i));
        }
        System.out.println();
    }

    private static int[][] arrayA(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                arr[i][j] = num;
                num++;
            }
        }
        return arr;
    }

    private static int[][] arrayB(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = 0; i < b; i++) {
            for (int j = 0; j < a; j++) {
                arr[j][i] = num;
                num++;
            }
        }
        return arr;
    }

    private static int[][] arrayV(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = 0; i < a; i++) {
            for (int j = b - 1; j >= 0; j--) {
                arr[i][j] = num;
                num++;
            }
        }
        return arr;
    }

    private static int[][] arrayG(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = 0; i < b; i++) {
            for (int j = a - 1; j >= 0; j--) {
                arr[j][i] = num;
                num++;
            }
        }
        return arr;
    }

    private static int[][] arrayD(int a, int b) {
        int[][] arr = new int[b][a];
        int num = 1;
        for (int i = 0; i < b; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < a; j++) {
                    arr[i][j] = num;
                    num++;
                }
            } else {
                for (int j = a - 1; j >= 0; j--) {
                    arr[i][j] = num;
                    num++;
                }
            }
        }
        return arr;
    }

    private static int[][] arrayE(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = 0; i < b; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < a; j++) {
                    arr[j][i] = num;
                    num++;
                }
            } else {
                for (int j = a - 1; j >= 0; j--) {
                    arr[j][i] = num;
                    num++;
                }
            }
        }
        return arr;
    }

    private static int[][] arrayJ (int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = a - 1; i >= 0; i--) {
            for (int j = 0; j < b; j++) {
                arr[i][j] = num;
                num++;
            }
        }
        return arr;
    }

    private static int[][] arrayZ(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = b - 1; i >= 0; i--) {
            for (int j = 0; j < a; j++) {
                arr[j][i] = num;
                num++;
            }
        }
        return arr;
    }

    private static int[][] arrayI(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = a - 1; i >= 0; i--) {
            for (int j = b - 1; j >= 0; j--) {
                arr[i][j] = num;
                num++;
            }
        }
        return arr;
    }

    private static int[][] arrayK(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = b - 1; i >= 0; i--) {
            for (int j = a - 1; j >= 0; j--) {
                arr[j][i] = num;
                num++;
            }
        }
        return arr;
    }

    private static int[][] arrayL(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = a - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = b - 1; j >= 0; j--) {
                    arr[i][j] = num;
                    num++;
                }
            } else {
                for (int j = 0; j < b; j++) {
                    arr[i][j] = num;
                    num++;
                }
            }
        }
        return arr;
    }

    private static int[][] arrayM(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = 0; i < a; i++) {
            if (i % 2 == 0) {
                for (int j = b - 1; j >= 0; j--) {
                    arr[i][j] = num;
                    num++;
                }
            } else {
                for (int j = 0; j < b; j++) {
                    arr[i][j] = num;
                    num++;
                }
            }
        }
        return arr;
    }

    private static int[][] arrayN(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = b - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = a - 1; j >= 0; j--) {
                    arr[j][i] = num;
                    num++;
                }
            } else {
                for (int j = 0; j < a; j++) {
                    arr[j][i] = num;
                    num++;
                }
            }
        }
        return arr;
    }

    private static int[][] arrayO(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = 0; i < b; i++) {
            if (i % 2 == 0) {
                for (int j = a - 1; j >= 0; j--) {
                    arr[j][i] = num;
                    num++;
                }
            } else {
                for (int j = 0; j < a; j++) {
                    arr[j][i] = num;
                    num++;
                }
            }
        }
        return arr;
    }

    private static int[][] arrayP(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = a - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < b; j++) {
                    arr[i][j] = num;
                    num++;
                }
            } else {
                for (int j = b - 1; j >= 0; j--) {
                    arr[i][j] = num;
                    num++;
                }
            }
        }
        return arr;
    }

    private static int[][] arrayR(int a, int b) {
        int[][] arr = new int[a][b];
        int num = 1;
        for (int i = b - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < a; j++) {
                    arr[j][i] = num;
                    num++;
                }
            } else {
                for (int j = a - 1; j >= 0; j--) {
                    arr[j][i] = num;
                    num++;
                }
            }
        }
        return arr;
    }
}

