package my_pack;

import java.util.Arrays;

/*
Дан двумерный массив.
а) Удалить из него k-ю строку.
б) Удалить из него s-й столбец.
 */

public class TaskCh12N234 {

    public static void main(String[] args) {

        int[][] inputArray = new int[][]
          {
            {5, 6, 18, 10, 15, 16, 8},
            {11, 12, 13, 14, 15, 25, 21},
            {11, 2, 3, 4, 5, 6, 77},
            {8, 88, 37, 14, 16, 0, 8},
            {85, 95, 5, 0, 1, 0, 92},
            {7, 10, 6, 0, 0, 77, 33},
            {15, 33, 70, 41, 55, 41, 71}
          };

        int[][] arrayWithRemovedRow = removeRow(inputArray, 7);
        int[][] arrayWithRemovedColumn = removeColumn(inputArray, 7);
        System.out.println("The array with removed row");
                printResult( arrayWithRemovedRow);
        System.out.println("The array with removed column");
                printResult(arrayWithRemovedColumn);
    }

    private static int[][] removeRow(int[][] array, int n) {
        if (n <= array.length) {
            for (int i = n - 1; i < array.length - 1; i++) {
                System.arraycopy(array[i + 1], 0, array[i], 0, array[i].length);
            }
            for (int j = 0; j < array[0].length; j++) {
                array[array.length - 1][j] = 0;
            }
        }
        return array;
    }

    private static int[][] removeColumn(int[][] array, int n) {
        if (n <= array[0].length) {
            for (int[] N : array) {
                System.arraycopy(N, n - 1 + 1, N, n - 1, N.length - 1 - (n - 1));
            }
            for (int i = 0; i < array.length; i++) {
                array[i][array[0].length - 1] = 0;
            }
        }
        return array;
    }


            private static void printResult(int[][] newArray) {
                for (int[] i : newArray) {
                    System.out.println(Arrays.toString(i));
                }
                System.out.println();
            }
        }